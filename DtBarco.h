#ifndef DTBARCO
#define DTBARCO
#include <string>
#include <iostream>
using namespace std;
class DtBarco {
  private:
    string nombre;
    string id;
  public:
    DtBarco();
    DtBarco(string nombre, string id);
    string getNombre();
    void setNombre(string nombre);
    string getId();
    void setId(string id);
    virtual ~DtBarco();

    friend ostream& operator <<(ostream&,const DtBarco&);
};
#endif
#include<iostream>
#include "Barco.h"
#include "DtBarco.h"

using namespace std;

//operacion b
void agregarBarco();
void agregarBarco(DtBarco&);

//operacion g
void obtenerBarcos();
DtBarco** obtenerBarcos(int&);

//auxiliares
void noExisteBarco(string);
void existeBarco(string;
Barco* obtenerBarco(string);
void nombreCorrecto(string);
void idCorrecto(string);

//operaciones auxiliares
void noExisteBarco(string id){
	int i=0;
	bool existe=false;
	while((i<coleccionBarcos.topeB)&&(!existe)){

		if(id == coleccionBarcos.barcos[i]->getid())
			existe=true;
		i++;
	}
	if (existe)
		throw invalid_argument("\nERROR: Existe Barco con el mismo ID.\n");
}

void existeBarco(string id){
	int i=0;
	bool existe=false;
	while((i<coleccionBarcos.topeB)&&(!existe)){

		if(id == coleccionBarcos.barcos[i]->getid())
			existe=true;
		i++;
	}
	if (!existe)
		throw invalid_argument("\nERROR: No existe ese Barco en el sistema.\n");
}

Barco* obtenerBarco(string id){
	Barco* BarcoObtenido;
	int i=0;
	bool existe=false;
	while((i<coleccionBarcos.topeB)&&(!existe)){
		if(id == coleccionBarcos.Barcos[i]->getid()){
			BarcoObtenido=coleccionBarcos.Barcos[i];
			existe=true;
		}
		i++;
	}
	return BarcoObtenido;
}

//void nombreCorrecto(string nombre){
//}
//
//void idCorrecto(string id){
//}


//Opeacion b
void agregarBarco(){
	system("clear");
	cout <<"_____________________________________________" <<endl;
	cout <<"______A G R E G A R_____ B A R C O _____"<< endl;
	string nombre, id;
	DtBarcoPasajeros dtpasajeros;
	DtBarcoPesquero dtpesquero;
	
	cout << "\nID: ";
	cin >> id;
	
	try{
		noExisteBarco(id);

		cout << "NOMBRE ";
		cin >> nombre;

        //
        //Realizar nombreCorrecto
        //Realizar idCorrecto
        //

	}catch(invalid_argument& e){
		cout << e.what() <<endl;
	}
}	

//Operacion g
void obtenerBarcos(){
	system("clear");
	cout <<"_________________________________________________" <<endl;
	cout <<"__V E R__L I S T A D O__ D E__B A R C O S __"<< endl;
	cout << endl;
	int cantBarcos;
	DtBarcoPasajeros* pasajeros;
	DtBarcoPesquero* pesquero;
	DtBarco** dtBarcos = obtenerBarcos(cantBarcos);
	for (int i = 0; i < cantBarcos; i++){
		pesquero = dynamic_cast<DtBarcoPesquero*>(dtBarcos[i]);
		if (pesquero!=NULL)
			cout << "\n\n" << *(pesquero);
		else{
			pasajeros = dynamic_cast<DtBarcoPasajeros*>(dtBarcos[i]);
			if (pasajeros!=NULL)
				cout << "\n\n" << *(pasajeros);	
		}
	}
	cout << "\nProcesando"<<endl;
	system("read X");
}


//Implementacion

//Operacion b
void agregarBarco(DtBarco& Barco){
    try{
        DtBarcoPasajeros& dtBarcoPasajeros = dynamic_cast<DtBarcoPasajeros&>(Barco);
        BarcoPasajeros* BarcoPasajeros = new BarcoPasajeros(dtBarcoPasajeros.getNombre(),dtBarcoPasajeros.getId(),dtBarcoPasajeros.getCantPasajeros(),dtBarcoPasajeros.getTamanio());
        coleccionBarcos.Barcos[coleccionBarcos.topeB]=BarcoPasajeros;
        coleccionBarcos.topeB++;
    }catch(bad_cast){
        try{
            DtBarcoPesquero& dtBarcoPesquero = dynamic_cast<DtBarcoPesquero&>(Barco);
            BarcoPesquero* BarcoPesquero = new BarcoPesquero(dtBarcoPesquero.getNombre(),dtBarcoPesquero.getId(),dtBarcoPesquero.getCapacidad(),dtBarcoPesquero.getCarga()); 
            coleccionBarcos.Barcos[coleccionBarcos.topeB]=BarcoPesquero;
            coleccionBarcos.topeB++;
        }catch(bad_cast){}    
    }
}

//Operacion g
DtBarco** obtenerBarcos(int& cantBarcos){
	DtBarco** dtBarcos = new DtBarco*[coleccionBarcos.topeB];
	DtBarcoPasajeros* dtpasajero;
	DtBarcoPesquero* dtpesquero;
	cantBarcos=coleccionBarcos.topeB;
	for(int i=0;i<cantBarcos;i++){
		if(Pasajero* pasajero = dynamic_cast<Pasajero*>(coleccionBarcos.Barcos[i])){
				dtpasajero = new DtBarcoPasajeros(pasajero.getNombre(),pasajero.getId(),pasajero.getCantPasajeros(),pasajero.getTamanio());
				dtBarcos[i]=dtpasajero;
		}else{
			if(Pesquero* pesquero = dynamic_cast<Pesquero*>(coleccionBarcos.Barcos[i])){
					dtpesquero = new DtBarcoPesquero(pesquero.getNombre(),pesquero.getId(),pesquero.getCapacidad(),pesquero.getCarga());
					dtBarcos[i]=dtpesquero;
			}
		}
	}
	return dtBarcos;
}

//Agregar en el main  coleccionBarcos.topeB=0;
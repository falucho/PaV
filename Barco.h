#ifndef BARCO
#define BARCO
#include<string>
#include<iostream>
using namespace std;

class Lugar {
  private:
    string nombre;
    string id;
  public:
    Barco();
    Barco(string nombre, string id);
    string getNombre();
    void setNombre(string nombre);
    string getId();
    void setId(string id);
    ~Barco();

    virtual float arribar(cargaDespacho)=0;
    virtual DtBarco getDtBarco() = 0;
};
#endif